package com.test;

import com.start.AppApplication;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertThat;

/**
 * ClassName: OutputCaptureTest
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/19
 */
@RunWith(SpringJUnit4ClassRunner.class)
//指定SpringBoot工程的Application启动类
//支持web项目
@WebAppConfiguration
@SpringBootTest(classes = AppApplication.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class OutputCaptureTest {
    private Log log = LogFactory.getLog(OutputCaptureTest.class);
    @Rule
    // 这里注意，使用e@Rule注解必须要用public
    public OutputCapture capture = new OutputCapture();

    @Test
    public void testLogger() {
        System.out.println("sd");
        log.info("test");
        assertThat(capture.toString(), Matchers.containsString("test"));
    }

    @Test
    public void testSdLogger() {
        System.out.println("sd");
        assertThat(capture.toString(), Matchers.containsString("sd"));
    }

    @Test
    public void testNoSdLogger() {
        System.out.println("sd");
        assertThat(capture.toString(), Matchers.containsString("nod"));
    }
}
