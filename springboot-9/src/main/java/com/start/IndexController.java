package com.start;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: IndexController
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/20
 */
@RestController
public class IndexController {
    @GetMapping("/index")
    public ResponseEntity jdbcTemplateDBCP2() {
        return ResponseEntity.ok("ok");
    }
}
