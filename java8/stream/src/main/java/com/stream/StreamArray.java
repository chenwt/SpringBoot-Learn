package com.stream;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * ClassName: StreamArray
 * Description:
 *
 * @author kang.wang03
 *         Date 2016/11/17
 */
public class StreamArray {
    public static void main(String args[]) {
        sort();
        maxAndMin();
        filter();
        map();
    }

    public static void filMap(){
        List<String> data = new ArrayList<String>();
        for (int i = 0; i < 50; i++) {
            data.add(new Random().nextInt(10000)+"a");
        }
        data = data.stream().map(String::toUpperCase).collect(Collectors.toList());
        List<Integer> integers = new ArrayList<>();
        System.out.println("map:" + Arrays.toString(data.toArray(new String[data.size()])));

    }
    public static void map() {
        List<String> data = new ArrayList<String>();
        for (int i = 0; i < 50; i++) {
            data.add(new Random().nextInt(10000)+"a");
        }
        data = data.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println("map:" + Arrays.toString(data.toArray(new String[data.size()])));

    }

    /**
     * 过滤
     */
    public static void filter() {
        List<Integer> data = new ArrayList<Integer>();
        for (int i = 0; i < 50; i++) {
            data.add(new Random().nextInt(10000));
        }
        data = data.stream().filter((obj) -> obj.toString().endsWith("2")).collect(Collectors.toList());
        System.out.println("filter:" + Arrays.toString(data.toArray(new Integer[data.size()])));
    }

    /**
     * 最大值最小值
     */
    public static void maxAndMin() {
        List<Integer> data = new ArrayList<Integer>();
        for (int i = 0; i < 50; i++) {
            data.add(new Random().nextInt(10000));
        }
        Optional<Integer> max = data.stream().max(((o1, o2) -> {
            return o1.compareTo(o2);
        }));
        Optional<Integer> min = data.stream().min(((o1, o2) -> {
            return o1.compareTo(o2);
        }));
        if (max.isPresent())
            System.out.println("max value :" + max.get());
        if (min.isPresent())
            System.out.println("max value :" + min.get());

    }

    /**
     * 排序
     */
    public static void sort() {
        List<Integer> data = new ArrayList<Integer>();
        for (int i = 0; i < 50; i++) {
            data.add(new Random().nextInt(10000));
        }
        data = data.stream().sorted((o1, o2) -> {
            return o1.compareTo(o2);
        }).collect(Collectors.toList());
        System.out.println(Arrays.toString(data.toArray(new Integer[data.size()])));
    }
}
